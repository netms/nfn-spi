"use strict";

let FN = {
	each:  function (value, callback) {
		if (value && typeof (callback) == "function") {
			for (let k in value) {
				if (callback(value[k], k, typeof(value[k])) === false) return false;
			}
		}

		return true;
	},
	has:   function (value, list) {
		return !this.each(list, d=> d !== value);
	},
	clone: function (dest, src) {
		this.each(src, (d, k)=> dest[k] = d);

		return dest;
	}
};

function NFN (oper, opt, EVENT) { this.init(oper, opt, EVENT); }

NFN.extend = function (src) {
	function dest () { this.init.apply(this, arguments); }

	dest.extend = NFN.extend;
	dest.prototype = NFN.prototype;

	FN.clone(dest.prototype, src);

	return dest;
};

NFN.prototype = {
	init:          function (opt, event, oper, EVENT) {
		this.oper = oper;
		this.opt = this.option(opt);
		this.state = false;
		this.event = {};
		this.middle = {};
		this.EVENT = EVENT || ["start", "stop", "read", "write"];

		this.func = (tag, data, own, i)=> {
			if (tag == "reset") {
				this.open();
				return;
			}

			data = [...data, tag];
			own = this.event[tag];
			for (i = 0; i < own.length; i++) own[i].apply(this, data);

			if (tag != "read") return;
			for (i in this.middle) this.middle[i].work.apply(this.middle[i], data);
		};

		FN.each(EVENT, d=> this.event[d] = []);

		FN.each(event, (d, k, t)=> this.on(k, d));

		this.init_custom.apply(this, arguments);

		return this;
	},
	init_custom:   function () { },
	option:        function (opt, def) {
		let {
				log, manu, hex,
				sleep, rmax, wmax, name
			}          = opt || {},
			_def       = def || {
					log:   false,
					manu:  false,
					hex:   true,
					sleep: 100,
					rmax:  512,
					wmax:  512,
					name:  ""
				};

		log = typeof(log) == "boolean" ? log === true : _def.log;
		manu = typeof(manu) == "boolean" ? manu === true : _def.manu;
		hex = typeof(hex) == "boolean" ? hex !== false : _def.hex;

		if (typeof(sleep) != "number" || sleep <= 0) sleep = _def.sleep;
		if (typeof(rmax) != "number" || rmax < 32) rmax = _def.rmax;
		if (typeof(wmax) != "number" || wmax < 32) wmax = _def.wmax;

		rmax = Math.min(rmax, 4096);
		wmax = Math.min(wmax, 4096);

		if (typeof(name) != "string" || name.length < 8) name = _def.name;

		return this.option_custom({log, manu, hex, sleep, rmax, wmax, name}, opt, def);
	},
	option_custom: function (dest, opt, def) { return dest; },
	config:        function (key, value) {
		let result;

		switch (arguments.length) {
			case 0:
				this.config_custom(0, this.opt);

				result = {
					name:   this.opt.name,
					speed:  this.opt.speed,
					parity: this.opt.parity,
					stop:   this.opt.stop,
					size:   this.opt.size
				};

				if (result.parity == 1) result.parity = "ODD";
				else if (result.parity == 2) result.parity = "EVEN";
				else result.parity = "NONE";

				return result;
			case 1:
				this.opt = this.option(key, this.opt);

				if (!this.state) return;

				this.reset();
				break;
			case 2:
				if (value === null || value === undefined) return;

				(result = {})[key] = value;
				this.opt = this.option(result, this.opt);

				if (!this.state) return;

				if (key == "log" || key === "hex" || key == "sleep") this.oper.config(this.hwnd, this.opt);
				else this.config_custom(2, this.opt, key, value);
				break;
		}
	},
	config_custom: function (mode, opt, key, value) {
		switch (mode) {
			case 0:
				break;
			case 2:
				this.reset();
				break;
		}
	},
	open:          function (name) {
		this.close();

		if (typeof(name) == "string" && name.length >= 8) this.opt.name = name;
		if (this.opt.name == "") return false;

		this.hwnd = this.oper.open(this.opt, this.func);
		this.state = this.hwnd > 0;

		return this.state;
	},
	close:         function () {
		if (this.state) this.oper.close(this.hwnd);

		this.state = false;
		this.hwnd = 0;
	},
	reset:         function () {
		if (this.state) this.oper.reset(this.hwnd);

		this.state = false;
		this.hwnd = 0;
	},
	on:            function (tag, func) {
		if (!tag) return this;

		if (arguments.length == 1 && typeof(tag) == "object") {
			func = tag;
			tag = func.tag || func.name;

			this.on(tag, func);
		}
		else if (func && FN.has(tag, this.EVENT)) {
			switch (typeof(func)) {
				case "function":
					this.event[tag].push(func);
					break;
				case "object":
					if (func.tag == tag && func.work) this.middle[tag] = func;
					break;
			}
		}

		return this;
	},
	off:           function (tag, func) {
		let e, i;

		if (arguments.length > 0 && typeof(tag) != "string") return this;

		switch (arguments.length) {
			case 2:
				if ((e = this.event[tag]).length) {
					i = 0;
					while (i < e.length) {
						if (Object.is(e[i], func)) e.splice(i, 1);
						else i++;
					}
				}
				break;
			case 1:
				if (this.middle[tag]) delete this.middle[tag];
				else if (this.event[tag]) this.event[tag] = [];
				break;
			default:
				this.middle = {};

				e = this.event;
				for (i in e) this.off(i);
				break;
		}

		return this;
	},
	clear:         function () {
		if (this.state) this.oper.clear(this.hwnd);
	},
	read:          function (len) {
		if (!this.state) return false;

		if (typeof(len) != "number" || len < 1) len = 4096;

		len = Math.min(len, this.opt.rmax);

		return this.read(len);
	},
	write:         function (value, len) {
		if (!this.state) return false;

		if (Buffer.isBuffer(value)) {
			if (typeof(len) != "number" || len < 1) len = value.length;

			if (len < value.length) value = value.slice(0, len);

			return this.oper.write(this.hwnd, value);
		}
		else {
			value = String(value);
			if (typeof(len) == "number" && len > 0 && len < value.length) value = value.slice(0, len);

			return this.write(new Buffer(value, "utf8"));
		}
	}
};

exports = module.exports = {fn: FN, NFN: NFN};