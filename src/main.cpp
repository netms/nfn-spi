#include <sys/ioctl.h>
#include <linux/spi/spidev.h>

#include "nfn.h"

namespace spi {
	using namespace nfn;

	struct spi_device : public nfn_device {
		uint32_t baud{500};
		uint8_t bit{8};
		uint16_t delay{3};
		bool cp_loop{false};
		bool cp_ol{false};
		bool cp_ha{false};
		bool lsb_first{false};
		bool cs_high{false};
		bool cs_no{false};
		bool ready_low{false};
		bool wire_3{false};
	};

	struct spi_device_pipe {
		uint32_t baud{500};
		uint8_t bit{8};
		uint16_t delay{3};
	};

	struct spi_info : public nfn_info {
		uint16_t delay;
		uint32_t baud;
		uint8_t bit;
	};

	class spi_nfn : public nfn_Core {
		protected:

			void init_custom(nfn_info *info, nfn_device *dev) {
				spi_info *_info = (spi_info *) info;
				spi_device *_dev = (spi_device *) dev;

				_info->state &= ~(NFN_UV);

				_info->delay = _dev->delay;
				_info->baud = _dev->baud;
				_info->bit = _dev->bit;
			}

			nfn_info *info_init(Isolate *isolate, Local<Object> opt) {
				return new spi_info;
			}

			Local<String> info_get(Isolate *isolate, nfn_device *dev) {
				spi_device *info = (spi_device *) dev;
				char str[24];
				int i = 0;

				memcpy(&str[i], info->cp_loop ? "T_" : "F_", 2);
				i += 2;

				memcpy(&str[i], info->cp_ha ? "T_" : "F_", 2);
				i += 2;

				memcpy(&str[i], info->cp_ol ? "T_" : "F_", 2);
				i += 2;

				memcpy(&str[i], info->lsb_first ? "T_" : "F_", 2);
				i += 2;

				memcpy(&str[i], info->ready_low ? "T_" : "F_", 2);
				i += 2;

				memcpy(&str[i], info->cs_high ? "T_" : "F_", 2);
				i += 2;

				memcpy(&str[i], info->cs_no ? "T_" : "F_", 2);
				i += 2;

				memcpy(&str[i], info->wire_3 ? "T_" : "F_", 2);
				i += 2;

				i = sprintf(&str[i], "%d_", (int) info->baud / 1000);
				i += 16;

				sprintf(&str[i], "%d", info->bit);

				return String::NewFromUtf8(isolate, &str[0]);
			}

			nfn_device *info_set_custom(Isolate *isolate, Local<Object> opt) {
				spi_device *dev = new spi_device;
				Local<String> key[] = {
						String::NewFromUtf8(isolate, "speed"),
						String::NewFromUtf8(isolate, "delay"),
						String::NewFromUtf8(isolate, "bit"),
						String::NewFromUtf8(isolate, "loop"),
						String::NewFromUtf8(isolate, "cpol"),
						String::NewFromUtf8(isolate, "cpha"),
						String::NewFromUtf8(isolate, "lsb"),
						String::NewFromUtf8(isolate, "cshi"),
						String::NewFromUtf8(isolate, "csno"),
						String::NewFromUtf8(isolate, "low"),
						String::NewFromUtf8(isolate, "wire3")
				};
				Local<Value> vlv;
				size_t vli;

				vlv = opt->Get(key[0]);
				if (vlv->IsNumber()) {
					vli = (size_t) vlv->NumberValue();
					if (vli > 0) dev->baud = (uint32_t) (vli * 1000);
				}

				vlv = opt->Get(key[1]);
				if (vlv->IsNumber()) {
					vli = (size_t) vlv->NumberValue();
					if (vli > 0) dev->delay = (uint16_t) vli;
				}

				vlv = opt->Get(key[2]);
				if (vlv->IsNumber()) {
					vli = (size_t) vlv->NumberValue();
					if (vli > 0) dev->bit = (uint8_t) vli;
				}

				vlv = opt->Get(key[3]);
				if (vlv->IsBoolean()) dev->cp_loop = vlv->BooleanValue();

				vlv = opt->Get(key[4]);
				if (vlv->IsBoolean()) dev->cp_ol = vlv->BooleanValue();

				vlv = opt->Get(key[5]);
				if (vlv->IsBoolean()) dev->cp_ha = vlv->BooleanValue();

				vlv = opt->Get(key[6]);
				if (vlv->IsBoolean()) dev->lsb_first = vlv->BooleanValue();

				vlv = opt->Get(key[7]);
				if (vlv->IsBoolean()) dev->cs_high = vlv->BooleanValue();

				vlv = opt->Get(key[8]);
				if (vlv->IsBoolean()) dev->cs_no = vlv->BooleanValue();

				vlv = opt->Get(key[9]);
				if (vlv->IsBoolean()) dev->ready_low = vlv->BooleanValue();

				vlv = opt->Get(key[10]);
				if (vlv->IsBoolean()) dev->wire_3 = vlv->BooleanValue();

				return dev;
			}

			bool device_set(nfn_device *dev) {
				spi_device *info = (spi_device *) dev;

				uint8_t val8 = 0;
				uint32_t val32 = 0;

				if (ioctl(info->hwnd, SPI_IOC_RD_MODE, &val8) == -1) {
					if (info->log) printf("[nfn-spi bin] get mode error\n");
					return false;
				}

				val8 &= SPI_MODE_0;
				if (info->cp_loop) val8 |= SPI_LOOP;
				if (info->cp_ha) val8 |= SPI_CPHA;
				if (info->cp_ol) val8 |= SPI_CPOL;
				if (info->lsb_first) val8 |= SPI_LSB_FIRST;
				if (info->cs_high) val8 |= SPI_CS_HIGH;

				if (info->cs_no) val8 |= SPI_NO_CS;
				if (info->ready_low) val8 |= SPI_READY;
				if (info->wire_3) val8 |= SPI_3WIRE;

				if (ioctl(info->hwnd, SPI_IOC_WR_MODE, &val8) == -1) {
					if (info->log) printf("[nfn-spi bin] set mode error\n");
					return false;
				};

				val8 = info->bit;
				if (ioctl(info->hwnd, SPI_IOC_WR_BITS_PER_WORD, &val8) == -1) {
					if (info->log) printf("[nfn-spi bin] set bit error\n");
					return false;
				}

				val32 = info->baud;
				if (ioctl(info->hwnd, SPI_IOC_WR_MAX_SPEED_HZ, &val32) == -1) {
					if (info->log) printf("[nfn-spi bin] set speed error\n");;
					return false;
				}

				return true;
			}

			int device_open(nfn_device *dev) {
				return open(dev->name, O_RDWR);
			}

		public:

			spi_device_pipe *parse_sdp(Isolate *isolate, Local<Object> opt) {
				spi_info *info = (spi_info *) this->_info;
				if (!info || (info->state & NFN_CLOSE)) return NULL;

				spi_device_pipe *dev = new spi_device_pipe;
				dev->baud = info->baud;
				dev->delay = info->delay;
				dev->bit = info->bit;

				Local<String> key[] = {
						String::NewFromUtf8(isolate, "speed"),
						String::NewFromUtf8(isolate, "delay"),
						String::NewFromUtf8(isolate, "bit")
				};
				Local<Value> vlv;
				size_t vli;

				vlv = opt->Get(key[0]);
				if (vlv->IsNumber()) {
					vli = (size_t) vlv->NumberValue();
					if (vli > 0) dev->baud = (uint32_t) (vli * 1000);
				}

				vlv = opt->Get(key[1]);
				if (vlv->IsNumber()) {
					vli = (size_t) vlv->NumberValue();
					if (vli > 0) dev->delay = (uint16_t) vli;
				}

				vlv = opt->Get(key[2]);
				if (vlv->IsNumber()) {
					vli = (size_t) vlv->NumberValue();
					if (vli > 0) dev->bit = (uint8_t) vli;
				}

				return dev;
			}

			Local<Value> write(Isolate *isolate, nfn_buffer *tx, spi_device_pipe *opt) {
				bool result = false;
				Local<Value> result_data;

				spi_info *info = (spi_info *) this->_info;

				if (info && !(info->state & NFN_CLOSE)) {
					nfn_buffer *rx = buffer_init(min(tx->max, 4096));

					spi_ioc_transfer sit;
					sit.tx_buf = (unsigned long) tx->data;
					sit.rx_buf = (unsigned long) rx->data;
					sit.len = rx->max;
					sit.delay_usecs = opt ? opt->delay : info->delay;
					sit.speed_hz = opt ? opt->baud : info->baud;
					sit.bits_per_word = opt ? opt->bit : info->bit;

					rx->len = ioctl(info->hwnd, SPI_IOC_MESSAGE(1), &sit);
					if (rx->len > 0) {
						result = true;
						result_data = BuftoHex(isolate, rx, true);

						memcpy((char *) info->read->buf->data, (char *) rx->data, rx->len);
						info->read->buf->len = rx->len;
						uv_async_send(&info->read->work);
					}

//					printf("[nfn-spi bin] pipe r: %s len: %d\n", result ? "true" : "false", rx->len);

					buffer_free(rx);
				}

				if (!result) result_data = Boolean::New(isolate, false);

				return result_data;
			}

			Local<Value> read(Isolate *isolate, spi_device_pipe *sdp) {
				nfn_buffer *buf = buffer_init(1);
				*(char *) buf->data = 0xff;

				Local<Value> result = this->write(isolate, buf, sdp);

				buffer_free(buf);

				return result;
			}
	};

//	api functions

	void openSpi(const FunctionCallbackInfo<Value> &arg) {
		Isolate *isolate = arg.GetIsolate();
		long hwnd = 0;

		if (arg.Length() == 2 && arg[0]->IsObject() && arg[1]->IsFunction()) {
			spi_nfn *obj = new spi_nfn();

			if (obj->start(isolate, Local<Object>::Cast(arg[0]), Local<Function>::Cast(arg[1]))) hwnd = (long) obj;
			else {
				delete obj;
			}
		}

		arg.GetReturnValue().Set(Number::New(isolate, hwnd));
	}

	void closeSpi(const FunctionCallbackInfo<Value> &arg) {
		if (arg.Length() != 1 || !arg[0]->IsNumber()) return;

		long hwnd = (long) Local<Number>::Cast(arg[0])->NumberValue();

		if (hwnd > 0) {
			spi_nfn *obj = (spi_nfn *) hwnd;
			delete obj;
		}
	}

	void resetSpi(const FunctionCallbackInfo<Value> &arg) {
		if (arg.Length() != 1 || !arg[0]->IsNumber()) return;

		long hwnd = (long) Local<Number>::Cast(arg[0])->NumberValue();

		if (hwnd > 0) ((spi_nfn *) hwnd)->reset();
	}

	void configSpi(const FunctionCallbackInfo<Value> &arg) {
		Isolate *isolate = arg.GetIsolate();
		bool result = false;

		if (arg.Length() == 2 && arg[0]->IsNumber() && arg[1]->IsObject()) {
			long hwnd = (long) Local<Number>::Cast(arg[0])->NumberValue();

			if (hwnd > 0) result = ((spi_nfn *) hwnd)->config(isolate, Local<Object>::Cast(arg[1]));
		}

		arg.GetReturnValue().Set(Boolean::New(isolate, result));
	}

	void clearData(const FunctionCallbackInfo<Value> &arg) {
		if (arg.Length() != 1 || !arg[0]->IsNumber()) return;

		long hwnd = (long) Local<Number>::Cast(arg[0])->NumberValue();

		if (hwnd > 0) ((spi_nfn *) hwnd)->clear();
	}

	void readData(const FunctionCallbackInfo<Value> &arg) {
		Isolate *isolate = arg.GetIsolate();
		bool result = false;
		Local <Value> result_data;

		if (arg.Length() >= 1 && arg[0]->IsNumber()) {
			long hwnd = (long) Local<Number>::Cast(arg[0])->NumberValue();

			if (hwnd > 0) {
				spi_nfn *spi = (spi_nfn *) hwnd;
				spi_device_pipe *sdp = arg.Length() == 2 && arg[1]->IsObject() ?
									   spi->parse_sdp(isolate, Local<Object>::Cast(arg[1])) :
									   NULL;

				result_data = spi->read(isolate, sdp);
				result = true;
			}
		}

		if (!result) result_data = Boolean::New(isolate, false);

		arg.GetReturnValue().Set(result_data);
	}

	void writeData(const FunctionCallbackInfo<Value> &arg) {
		Isolate *isolate = arg.GetIsolate();
		bool result = false;
		Local <Value> result_data;

		if (arg.Length() >= 2 && arg[0]->IsNumber() && arg[1]->IsUint8Array()) {
			long hwnd = (long) Local<Number>::Cast(arg[0])->NumberValue();
			Local <Uint8Array> data = Local<Uint8Array>::Cast(arg[1]);

			if (hwnd > 0 && data->ByteLength() > 0) {
				spi_nfn *spi = (spi_nfn *) hwnd;
				spi_device_pipe *sdp = arg.Length() == 3 && arg[2]->IsObject() ?
									   spi->parse_sdp(isolate, Local<Object>::Cast(arg[2])) :
									   NULL;

				nfn_buffer buf;
				buf.data = (void *) HextoPChar(data);
				buf.max = data->ByteLength();
				buf.len = data->ByteLength();

				result_data = spi->write(isolate, &buf, sdp);
				result = true;
			}
		}

		if (!result) result_data = Boolean::New(isolate, false);

		arg.GetReturnValue().Set(result_data);
	}

	void Initialize(Handle<Object> exports, Handle<Object> module) {
		NODE_SET_METHOD(exports, "open", openSpi);
		NODE_SET_METHOD(exports, "close", closeSpi);
		NODE_SET_METHOD(exports, "reset", resetSpi);
		NODE_SET_METHOD(exports, "config", configSpi);
		NODE_SET_METHOD(exports, "clear", clearData);
		NODE_SET_METHOD(exports, "read", readData);
		NODE_SET_METHOD(exports, "write", writeData);
	}

	NODE_MODULE(spi, Initialize
	)
}