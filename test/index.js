"use strict";

let spi                                    = require("../index.js"),
	[name = "/dev/spidev0.0", speed = 131] = process.argv.splice(2),
	oper;

oper = spi.init({
	log:   true,
	hex:   false,
	name:  name,
	speed: speed,
	bit:   8,
	delay: 0,
	cpol:  false,
	cpha:  false,
	lsb:   false,
	cshi:  false
});

oper.on({
	"start": (name, cfg, tag)=> console.log(`[open] name: ${name} cfg: ${cfg}\n`),
	"stop":  ()=> console.log("[close]"),
	"read":  (buf, len)=> { console.log("[buf] ", buf); }
});

if (!oper.open()) {
	console.log("打开设备错误");
	process.exit();
}

oper.write("this is test");
