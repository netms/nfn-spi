"use strict";

module.exports = exports = ((spi, nfn)=> {
	let FN  = nfn.fn,
		NFN = nfn.NFN;

	let Spi = NFN.extend({
		init_custom:   function (opt, event, oper, EVENT, BIT) {
			this.BIT = BIT;
		},
		option_custom: function (dest, opt, def) {
			let {
					speed, delay, bit,
					loop, cpol, cpha, lsb,
					cshi, csno, low, wire3
				}          = opt || {};

			def = def || {
					speed: 1000,
					bit:   8,
					delay: 3,
					loop:  false,
					cpol:  false,
					cpha:  false,
					lsb:   false,
					cshi:  false,
					csno:  false,
					low:   false,
					wire3: false
				};

			dest.manu = opt.manu !== false;

			if (typeof(speed) != "number" || speed <= 0) speed = def.speed;
			if (typeof(delay) != "number" || delay < 1) delay = def.delay;
			if (typeof(bit) != "number" || FN.has(bit, this.BIT)) bit = def.bit;

			loop = typeof(loop) == "boolean" ? loop === true : def.loop;
			cpol = typeof(cpol) == "boolean" ? cpol === true : def.cpol;
			cpha = typeof(cpha) == "boolean" ? cpha === true : def.cpha;
			lsb = typeof(lsb) == "boolean" ? lsb === true : def.lsb;
			cshi = typeof(cshi) == "boolean" ? cshi === true : def.cshi;
			csno = typeof(csno) == "boolean" ? csno === true : def.csno;
			low = typeof(low) == "boolean" ? low === true : def.low;
			wire3 = typeof(wire3) == "boolean" ? wire3 === true : def.wire3;

			if (csno) cshi = false;

			Object.assign(dest, {speed, delay, bit, loop, cpol, cpha, lsb, cshi, csno, low, wire3});

			return dest;
		},
		config_custom: function (mode, opt, key, value) {
			let result;

			switch (mode) {
				case 0:
					result = {
						name:  opt.name,
						speed: opt.speed,
						delay: opt.delay,
						bit:   opt.bit,
						loop:  opt.loop,
						cpha:  opt.cpha,
						cpol:  opt.cpol,
						lsb:   opt.lsb,
						low:   opt.low,
						cshi:  opt.cshi,
						csno:  opt.csno,
						wire3: opt.wire3
					};

					return result;
				case 2:
					this.reset();
					break;
			}
		},
		read:          function (opt) {
			return this.state ? this.oper.read(this.hwnd, opt) : false;
		},
		write:         function (value, len, opt) {
			if (!this.state) return false;

			if (arguments.length == 2 && typeof(len) == "object") {
				opt = len;
				len = 0;
			}

			if (Buffer.isBuffer(value)) {
				if (typeof(len) != "number" || len < 1) len = value.length;

				if (len < value.length) value = value.slice(0, len);

				return this.oper.write(this.hwnd, value, opt);
			}
			else {
				value = String(value);
				if (typeof(len) == "number" && len > 0 && len < value.length) value = value.slice(0, len);

				return this.write(new Buffer(value, "utf8"), opt);
			}
		}
	});

	let init   = function (opt, event) {
			return new Spi(opt, event,
				spi,
				["start", "stop", "read"],
				[8, 16, 32, 64]);
		},
		result = function (opt, event) { return init(opt, event); };

	result.init = init;

	return result;
// })(require("./bin/spi.node"), require("./src/nfn.js"));
})(require("./src/build/Release/spi.node"), require("./src/nfn.js"));